# file-update



## Getting started

The application is hosted on a cluster on [MongoDB Atlas](https://www.mongodb.com/atlas) platform that uses Node.js driver.


## Test
Use the following url to test API with an api-key: `M61tD2hbG2YL2evTyn7E9UVr8GBGfHbNUctIOvGmFbBD0mmfk40SmKxlITMIGBqI`. Use api-key in the header part of the request.

```
https://data.mongodb-api.com/app/application-0-crnmm/endpoint/batch_upload

```

Sample request body:
```bash
[
    {
        "id": "${BUCKET_ID}-1",
        "status": "completed",
        "path": "s3://bucket/1.jpg"
    },
    {
        "id": "${BUCKET_ID}-2",
        "status": "completed",
        "path": "s3://bucket/2.jpg"
    },
    {
        "id": "${BUCKET_ID}-3",
        "status": "completed",
        "path": "s3://bucket/3.jpg"
    }
]
```
