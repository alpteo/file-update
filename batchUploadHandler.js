const PARTNER_BASE_URI = "https://pxlnwwd545.execute-api.us-east-2.amazonaws.com/api/buckets";
const BUCKET_ID = "v8r1lmWfL6KP";

// Custom Success and Error response codes
const SuccessResponse = {
  code: "Success",
  status: 200
};

const BadRequestError = {
  code: "BadRequestError",
  status: 400
};

const PartnerAPIError = {
  code: "PartnerAPIError",
  status: 500
};

function usePartnerEndpoint(bucketId) {
  const bucketFilesUrl = `${PARTNER_BASE_URI}/${bucketId}/files`;
  
  const getAllResourcesEndpoint = () => bucketFilesUrl;
  const getResourceByIdEndpoint = (id) => `${bucketFilesUrl}/${id}`
  
  return {
    getAllResourcesEndpoint,
    getResourceByIdEndpoint
  };
}

function prepareResponseBody(code, message) {
  return JSON.stringify({ code, message });
}

// This function is the endpoint's request handler.
exports = async function({ body }, response) {
    // Http client
    const axios = require("axios");
    
    response.setHeader('Content-Type', 'application/json');
    
    const reqBody = body ? JSON.parse(body.text()) : undefined;
    
    if (!reqBody || !reqBody.length) {
      response.setStatusCode(BadRequestError.status);
      response.setBody(prepareResponseBody(BadRequestError.code, reqBody));
      return;
    }

    const { getAllResourcesEndpoint, getResourceByIdEndpoint } = usePartnerEndpoint(BUCKET_ID);

    try {
      // Fetch all files from the partners API
      const { data: files } = await axios.get(getAllResourcesEndpoint());
      const fileHashMap = {};
      const patchOperationPromiseList = [];

      files.forEach((file) => {
        fileHashMap[file.id] = file;
      });
      
      const successFileIdList = [];
      const failedFileIdList = [];

      for (const file of reqBody) {
        if (!fileHashMap[file.id]) {
          response.setStatusCode(BadRequestError.status);
          response.setBody(prepareResponseBody(BadRequestError.code, `File with id ${file.id} does not exist in partners data.`));
          return;
        }

        try {
          const patchOperationResult = await axios.patch(getResourceByIdEndpoint(file.id), { status: file.status, path: file.path });
          successFileIdList.push(file.id);
        } catch (e) {
          failedFileIdList.push(file.id);
        }
      }

      const bodyMessage = `${successFileIdList.length}/${reqBody.length} is successfully updated.`;

      response.setStatusCode(SuccessResponse.status);
      response.setBody(prepareResponseBody(SuccessResponse.code, { details: bodyMessage, successFileIdList, failedFileIdList }));
    } catch (e) {
      response.setStatusCode(PartnerAPIError.status);
      response.setBody(
        prepareResponseBody(PartnerAPIError.code, `Couldn't get files from partner API: ${PARTNER_BASE_URI}. Error: ${e.message}`)
      );
    }
};
